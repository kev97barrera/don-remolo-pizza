import create from 'zustand';

import { getProducts } from '../helpers/calls-api/get-products';
import { IProduct } from '../helpers/models/product';


interface ProductsStore {
    products: IProduct[],
    cartProducts: IProduct[],
    categories: string[],
    total: number,
}

export const useStore = create((set: any) => ({
    products: [],
    cartProducts: [],
    categories: [],
    total: 0,

    loadProducts: async() => {
        const products: any[] = await getProducts();
        const categories = Array.from(new Set(products.map(item => item.category)));
        set({products, categories});
    },
    addProductToCart: (id: number | string) => set((state: ProductsStore) => {
        let newCartProducts = [];
		if(state.cartProducts.some(item => item.id === id)) {
			newCartProducts = state.cartProducts?.map((item) => (
				item.id === id ?
				{...item, stock: item.stock ? +item.stock+1 : 1} :
				item
			));
		} else {
            const productAdded = state.products.find(item => item.id === id);
            newCartProducts = [...state.cartProducts, {...productAdded, stock: 1}];

		}
        const totalByProduct = newCartProducts?.map(item => item.stock && item.price ? +item.stock * item.price : item.price);
        const newTotal = totalByProduct?.reduce((acc: number, current) => current ? acc+current: acc, 0);
        return {cartProducts: newCartProducts, total: newTotal}
    }),
    removeProductFromCart: (id: number | string) => set((state: ProductsStore) => {
        let newCartProducts = [];
        const product = state.cartProducts.find(item => item.id === id);
		if(product && product.stock && product.stock > 1) {
			newCartProducts = state.cartProducts?.map((item) => (
				item.id === id ?
				{...item, stock: item.stock ? +item.stock-1 : 0} :
				item
			));
		} else {
            newCartProducts = state.cartProducts.filter(item => item.id !== id);
		}
        const totalByProduct = newCartProducts?.map(item => item.stock && item.price ? +item.stock * item.price : item.price);
        const newTotal = totalByProduct?.reduce((acc: number, current) => current ? acc+current: acc, 0);
        return {cartProducts: newCartProducts, total: newTotal}
    }),
    clearCart: () => set(({cartProducts: [], total: 0}))
}));