import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Typography from '@mui/material/Typography';
import CloseIcon from '@mui/icons-material/Close';
import DeleteIcon from '@mui/icons-material/Delete';
import Box from '@mui/material/Box';

import { useStore } from '../store/store';
import { IProduct } from '../helpers/models/product';

import CheckoutProduct from './CheckoutProduct';
import { useStyles } from '../styles/mui-styles';

const Checkout = ({ open, fullScreen, handleClose }: CheckoutProps) => {

    const { cartProducts, total, clearCart } = useStore();

    const classes = useStyles()

    return (
        <Dialog
            fullScreen={fullScreen}
            open={open}
            onClose={handleClose}
        >
            <DialogTitle id="responsive-dialog-title" sx={{ display: 'flex', justifyContent:'space-between', alignItems: 'center'}}>
                <Box sx={{ display: 'flex', alignItems: 'center'}}>
                    <CloseIcon onClick={handleClose} sx={{mr:2}} fontSize='small'/>
                    <Typography variant='subtitle1'>Listado del pedido</Typography>
                </Box>
                <DeleteIcon
                    sx={{}}
                    fontSize='medium'
                    onClick={() => {
                        clearCart() 
                        handleClose()
                    }
                }/>
            </DialogTitle>
            <DialogContent sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
                <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                    {
                        cartProducts.length ? 
                        cartProducts.map((product: IProduct) => (
                            <CheckoutProduct key={product.id} product={product} />
                        )) :
                        <></>
                    }
                </Box>
                <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                    <Typography variant="subtitle1" color="text.secondary" sx={{ alignSelf: 'end' }}>Items totales ({cartProducts.map((item: IProduct) => item.stock).reduce((acc: number, current) => current ? acc+current: acc, 0)})</Typography>
                    <Typography sx={{mt: 2, ml: 1}} variant='h6' color="text.primary">
                        {
                            cartProducts.length ?
                            <>
                                {total.toLocaleString('en-US', {style: 'currency', currency: 'USD'})}
                            </> :
                            '$ 0.00'
                        }
                    </Typography>
                </Box>
            </DialogContent>
            <DialogActions sx={{ display: 'flex', justifyContent: 'center' }}>
                <Button className={classes.checkout_btn} onClick={handleClose}>
                    Ir al checkout
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default Checkout

interface CheckoutProps {
    open: boolean,
    fullScreen: boolean,
    handleClose: any
}