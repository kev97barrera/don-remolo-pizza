import { useEffect, useState } from 'react'
import Product from './Product'
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import { useStore } from '../store/store';
import { IProduct } from '../helpers/models/product';

const ProductsList = ({ category }: ProductsListProps) => {

    const { products } = useStore();
    const [ categoryProducts, setCategoryProducts ] = useState<IProduct[]>([]);

    useEffect(() => {
        const matchedProducts = products.filter((item: IProduct) => item.category === category);
        setCategoryProducts(matchedProducts);
    }, [category]);

    return (
        <Box sx={{ flexGrow: 1 }}>
            <Grid container spacing={2} justifyContent="center">
                {
                    categoryProducts.length ?
                    categoryProducts.map(product => (
                        <Grid key={product.id} item xs={6} md={3}>
                            <Product product={product}/>
                        </Grid>
                    )) :
                    <span>No products on category {category}</span>
                }
            </Grid>
        </Box>
    )
}

export default ProductsList

interface ProductsListProps {
    category: string
}