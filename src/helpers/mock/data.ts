import { IProduct } from "../models/product";

export const data: IProduct[] = [
    { 
        id: 1, 
        name: 'Hawaiana',
        category: 'Pizzas',
        price: 100,
        image: 'https://www.recetin.com/wp-content/uploads/2015/05/pizza_hawaiana.jpg'
    },
    { 
        id: 2, 
        name: 'Mexicana',
        category: 'Pizzas',
        price: 150,
        image: 'https://dam.cocinafacil.com.mx/wp-content/uploads/2020/12/pizza-a-la-mexicana.jpg'
    },
    { 
        id: 3, 
        name: 'Cuatro quesos',
        category: 'Pizzas',
        price: 120,
        image: 'https://vod-hogarmania.atresmedia.com/cocinatis/images/images01/2019/04/11/5caf542f1f4daa0001932466/1239x697.jpg'
    },
    { 
        id: 4, 
        name: 'Especial',
        category: 'Pizzas',
        price: 200,
        image: 'https://www.recetaspizzas.com/base/stock/Recipe/55-image/55-image_web.jpg'
    },
    { 
        id: 5, 
        name: 'Agua natural',
        category: 'Bebidas',
        price: 50,
        image: 'https://www.ororadio.com.mx/noticias/wp-content/uploads/2019/01/Agua-Natural.jpg'
    },
    { 
        id: 6, 
        name: 'Agua de jamaica',
        category: 'Bebidas',
        price: 60,
        image: 'https://cdn2.cocinadelirante.com/sites/default/files/styles/gallerie/public/images/2018/05/como-hacer-agua-con-flor-de-jamaica.jpg'
    },
    { 
        id: 7, 
        name: 'Refresco',
        category: 'Bebidas',
        price: 60,
        image: 'https://mejorconsalud.as.com/wp-content/uploads/2015/12/Refrescos.jpg'
    },
    { 
        id: 8, 
        name: 'Agua mineral',
        category: 'Bebidas',
        price: 60,
        image: 'https://dam.cocinafacil.com.mx/wp-content/uploads/2020/03/AGUA-MINERAL.jpg'
    },
];
