import { data } from "../mock/data";
import { db } from '../../firebase';
import { onSnapshot, collection } from "firebase/firestore";
import { IProduct } from "../models/product";
/**
 * getProducts function
 * @returns new Promise to resolve data
 * @todo uncomment lines 12 to 18 and comment 20 to 27 to use mock data
 * @todo uncomment lines 20 to 27 and comment 12 to 18 to use firestore data
 */

// export const getProducts = (): Promise<IProduct[]> => {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             resolve(data);
//         }, 1000);
//     });
// }

export const getProducts = (): Promise<IProduct[]> => {
    return new Promise((resolve, reject) => {
        onSnapshot(collection(db, 'products'),(snapshot) => {
            const products = snapshot.docs.map(doc => ({...doc.data(), id: doc.id} as IProduct ));
            resolve(products);
        });
    });
}