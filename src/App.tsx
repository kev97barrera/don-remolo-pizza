import './App.css';
import { useEffect, useState } from 'react';
import Container from '@mui/material/Container';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import LocalPizzaIcon from '@mui/icons-material/LocalPizza';
import LocalBarIcon from '@mui/icons-material/LocalBar';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Fab from '@mui/material/Fab';
import ShoppingCartCheckoutIcon from '@mui/icons-material/ShoppingCartCheckout';
import useMediaQuery from '@mui/material/useMediaQuery';
import LinearProgress from '@mui/material/LinearProgress';
import { useTheme } from '@mui/material/styles';

import Checkout from './components/Checkout';
import ProductsList from './components/ProductsList';
import SearchAppBar from './components/SearchAppBar';

import { useStore } from './store/store';
import { IProduct } from './helpers/models/product';
import { useStyles } from './styles/mui-styles';

function App() {

    const { cartProducts, categories, total, loadProducts } = useStore();
    const [ loading, setLoading ] = useState(true);
    const [ currentCategory, setCurrentCategory ] = useState(0);
    const [ open, setOpen ] = useState(false);
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

    const handleClose = () => {
        setOpen(false);
    };

    const style = {
        margin: 0,
        top: 'auto',
        right: 20,
        bottom: 20,
        left: 'auto',
        position: 'fixed',
    };

    useEffect(() => {
        loadProducts();
    }, [])

    useEffect(() => {
        if(categories.length) setLoading(false);
    }, [categories])

    return (
        <>
            <SearchAppBar />
            {
                !loading ?
                <Container sx={{mt: 2}}>
                    <Typography variant='h4' color="text.primary">
                        { fullScreen ? categories[currentCategory] : 'Categorias' }
                    </Typography>
                    <Typography variant='subtitle2' color="text.secondary">
                        Elige de nuestras deliciosas {categories[currentCategory]}
                    </Typography>
                    <Box sx={{ flexGrow: 1, mb: 2, mt: 2 }}>
                        <Tabs value={currentCategory} onChange={(e, newCategory) => setCurrentCategory(newCategory)}>
                            {categories.length ?
                                categories.map(category => (
                                    <Tab
                                        key={category}
                                        icon={category === 'Pizzas' ? <LocalPizzaIcon  fontSize="large"/> : <LocalBarIcon fontSize="large"/>}
                                    />
                                )) :
                            <></>}
                        </Tabs>
                    </Box>
                    <ProductsList category={categories[currentCategory]}/>
                    <Box sx={style} >
                        <Fab variant="extended" color='secondary' onClick={() => setOpen(true)} disabled={!cartProducts.length}>
                            <Typography variant='caption'>
                                { cartProducts.length ? <>
                                    {cartProducts.map((item: IProduct) => item.stock).reduce((acc: number, current) => current ? acc+current: acc, 0)} products <br /> 
                                    {total.toLocaleString('en-US', {style: 'currency', currency: 'USD'})}
                                </> : 'No products'}
                            </Typography>
                            <ShoppingCartCheckoutIcon sx={{ mr: 1, ml: 3 }} />
                        </Fab>
                    </Box>
                    <Checkout open={open} fullScreen={fullScreen} handleClose={handleClose}/>
                </Container> :
                <Box sx={{ width: '100%' }}>
                    <LinearProgress color='secondary'/>
                </Box>
            }
        </>
    );
}

export default App;
