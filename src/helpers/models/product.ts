export interface IProduct {
    id?: string | number,
    name: string,
    category: string,
    price: number,
    image: string,
    stock?: number,
}