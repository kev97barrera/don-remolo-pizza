# Pizzeria Don Remolo - Nuclea Solutions Challenge

This project was developed to face a front-end developer challenge with React from Nuclea Solutions, Kevin Barrera Hernandez.
Here the [app requirements](https://nucleasolutions.notion.site/Pizzer-a-a7cfa9f575df4fafaa524a9e701f01eb)

## Before run the application

Please run first `npm install` to install third libs
- Material UI
- Zustand
- Firebase

## Comment about Data

You can use Mock Data declared within the front-end project modifying `helpers/calls-api/get-products.ts` file.
This includes 2 `getProducts()` functions, one of them uses Mock data and the other uses Data fetched from Firebase/firstore.
Please 
- uncomment lines 12 to 18 and comment 20 to 27 to use mock data, or
- uncomment lines 20 to 27 and comment 12 to 18 to use firestore data

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

