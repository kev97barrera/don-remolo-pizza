import { useStyles } from '../styles/mui-styles';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import IconButton from '@mui/material/IconButton';
import AddIcon from '@mui/icons-material/Add';
import Typography from '@mui/material/Typography';
import { useStore } from '../store/store';

const Product = ({ product }: any) => {

    const { id, name, price, category, image } = product;

    const { addProductToCart } = useStore();

    const classes = useStyles();

    return (
        <Card>
            <CardMedia
                component="img"
                height="170"
                image={image}
                alt={category}
            />
            <CardContent sx={{pb: 0}}>
                <Typography gutterBottom variant="subtitle1" component="div" sx={{mb:0}}>
                    { name }
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    Price: { price.toLocaleString('en-US', {style: 'currency', currency: 'USD'}) }
                </Typography>
            </CardContent>
            <CardActions sx={{ display: 'flex', justifyContent: 'end', pt: 0 }}>
                <IconButton className={classes.btn} color="secondary" onClick={() => addProductToCart(id)}>
                    <AddIcon />
                </IconButton>
            </CardActions>
        </Card>
    )
}

export default Product