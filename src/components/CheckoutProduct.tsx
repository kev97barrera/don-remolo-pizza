import { useStyles } from '../styles/mui-styles';

import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import Fab from '@mui/material/Fab';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { useStore } from '../store/store';
import { Button } from '@mui/material';

const CheckoutProduct = ({ product }: any) => {

    const { addProductToCart, removeProductFromCart } = useStore();

    const { image, name, price, stock, id } = product;

    const classes = useStyles();

    return (
        <Card key={product.id} sx={{ display: 'flex', borderRadius: 5, height: 100, mb: 1, backgroundColor: '#F6F6F6' }}>
            <CardMedia
                component="img"
                sx={{ width: 60, height: 60, alignSelf: 'center', m: 2, borderRadius: 2 }}
                image={image}
            />
            <CardContent sx={{ flex: '1 0 auto', pl: 0, pr: 0 }}>
                <Typography component="div" variant="subtitle2">
                    {name}
                </Typography>
                <Typography variant="subtitle2" color="text.secondary" component="div">
                    Price: {price.toLocaleString('en-US', { style: 'currency', currency: 'USD' })}
                </Typography>
            </CardContent>
            <Box sx={{ display: 'flex', alignItems: 'center', p: 0 }}>
                <IconButton onClick={() => removeProductFromCart(id)}>
                    <RemoveIcon />
                </IconButton>
                <div className={classes.stock_div}>
                    {stock}
                </div>
                <IconButton onClick={() => addProductToCart(id)}>
                    <AddIcon />
                </IconButton>
            </Box>
        </Card>
    )
}

export default CheckoutProduct