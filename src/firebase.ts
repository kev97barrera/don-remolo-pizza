import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyCOgFUQ8u2P4KImdpmaHyQ4JzHNNTMqpMU",
    authDomain: "don-remolo-pizza.firebaseapp.com",
    projectId: "don-remolo-pizza",
    storageBucket: "don-remolo-pizza.appspot.com",
    messagingSenderId: "294035111092",
    appId: "1:294035111092:web:471eb774c9de016719abe1"
};

const app = initializeApp(firebaseConfig);
export const db = getFirestore();