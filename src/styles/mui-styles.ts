import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
    btn: {
        color: '#DDD',
        border: '2px solid #DDD',
        borderRadius: 15
    },
    stock_div: {
        display: 'flex',
        width: '25px',
        height: '25px',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#A6A6A6',
        border: '2px solid #A6A6A6',
        borderRadius: 10,
        color: 'white',
        fontSize: '12px'
    },
    checkout_btn: {
        width: '85%',
        height: '50px',
        backgroundColor: '#5C5C5C',
        borderRadius: 25,
        color: 'white'
    }
});